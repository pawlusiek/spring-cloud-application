package com.example.springcloudserverside;

import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping(value = "/whoami", produces = TEXT_PLAIN_VALUE)
    public String whoami() {
        return "Hello! I'm Config Server";
    }

}
