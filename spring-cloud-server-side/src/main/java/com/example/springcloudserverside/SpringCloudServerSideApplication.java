package com.example.springcloudserverside;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class SpringCloudServerSideApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudServerSideApplication.class, args);
    }

}
