package com.example.springcloudclientside;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringCloudClientSideApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudClientSideApplication.class, args);
	}

}
