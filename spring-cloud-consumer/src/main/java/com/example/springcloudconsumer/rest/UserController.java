package com.example.springcloudconsumer.rest;

import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class UserController {

    @Value("${user.role}")
    String userRole;

    @Autowired
    SpringCloudClientSideClient client;

    @Qualifier("eurekaClient")
    @Autowired
    @Lazy
    private EurekaClient eurekaClient;

    @Value("${spring.application.name}")
    private String appName;

    @GetMapping(value = "/whoami", produces = TEXT_PLAIN_VALUE)
    public String whoami() {
        return "Hello! I'm " + userRole;
    }

    @GetMapping(value = "/greeting", produces = TEXT_PLAIN_VALUE)
    public String greeting() {
        return String.format(
            "Hello from %s to '%s'!", eurekaClient.getApplication(appName).getName(), client.greeting());
    }
}
