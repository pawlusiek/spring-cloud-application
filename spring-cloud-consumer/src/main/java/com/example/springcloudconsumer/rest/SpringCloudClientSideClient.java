package com.example.springcloudconsumer.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("spring-cloud-client-side")
public interface SpringCloudClientSideClient {

    @RequestMapping("/greeting")
    String greeting();
}
